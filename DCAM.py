from keras import backend as K
from keras.layers import Layer
import numpy as np
class DCAM(Layer):
    """
    This layer exploit the Dual-Chamber Absorption model to make the sparse data continuus.


    """

    def __init__(self, sparse_features= [1], **kwargs):
        self.sparse_features = sparse_features
        super(DCAM, self).__init__(**kwargs)

    def build(self, input_shape):
        n = len(self.sparse_features)

        # Trainable weights, 2 for each input column
        init=np.ones((input_shape[-1],2),dtype='float32')
        init[:,1]=0.5
        self.kernel = K.variable(init)
        self.trainable_weights=[self.kernel]
        super(DCAM, self).build(input_shape) 

    def dexp(self,data):
        equation = lambda x, t, k1,k2: x * -k1 * ((K.exp(-k1 * t) - K.exp(-k2 * t)) / (k1 - k2))

        l=data.shape[1] #number of timesteps
        channels=data.shape[-1] 
        data=K.temporal_padding(data,padding=(0,l-1)) #causal zero padding
        data = K.expand_dims(data,axis=1) #extra dimension for use with conv2d
        imp = K.variable(K.zeros((l,channels)))
        i=K.expand_dims(K.constant(list(range(0,l))))#time vector
        imp=equation(1, i,K.expand_dims(self.kernel[:,0],axis=0),K.expand_dims(self.kernel[:,1],axis=0))
        #calculates the impulse response

        mask=np.zeros((l,channels,channels))#masks only the selected channels
        for i in range(0,channels):
            mask[:, i, i] = 1

        fsel_w=np.zeros((l,channels))
        fsel_w[:,self.sparse_features]=1
        fsel_w=K.variable(fsel_w,name='fsel_w')

        fsel_b=np.zeros((l,channels))
        fsel_b[0,:]=1
        fsel_b[0,self.sparse_features]=0
        fsel_b = K.variable(fsel_b, name='fsel_b')

        imp=imp*fsel_w+fsel_b
        imp=K.expand_dims(imp)
        imp=K.repeat_elements(imp,channels,-1)
        imp=imp*mask#applyes the mask
        imp=K.expand_dims(imp,axis=0) #extra dimension for use with conv2d
        #padding was already made manually
        c = K.conv2d(data, imp, padding="valid", strides=(1,1),data_format='channels_last')

        c=K.squeeze(c,axis=1)#remove extra dimension
        return c

    def call(self, x):
        return self.dexp(x)

